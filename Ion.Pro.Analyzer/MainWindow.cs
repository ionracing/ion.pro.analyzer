﻿using Ion.Data.Networking.Manager;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Events;
using Ion.Graphics.IonEngine.Font;
using Ion.Graphics.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.Analyzer
{
    class MainWindow : GuiWindow
    {
        SDLRenderer mainRenderer;
        KeyboardState prev;
        KeyboardState cur;

        GraphDisplay graphDisplay_speed;
        GraphDisplay graphDisplay_soc;
        GraphDisplay graphDisplay_tempbat;
        GraphDisplay graphDisplay_tempcool;
        FileNetworkClient fileClient;

        public MainWindow()
        {

            mainRenderer = SDLRenderer.Create(this);

            FileInfo fontFile = new FileInfo("Content/unispace.ttf");
            Global.Font = SDLFont.LoadFont(fontFile.FullName, 50);
            InitializeComponents(mainRenderer);
            ShowCursor = true;
            string logDir = "\\home\\pi\\log\\";

            graphDisplay_speed.DotSpan = 1;
            graphDisplay_soc.DotSpan = 1;
            graphDisplay_tempbat.DotSpan = 1;
            graphDisplay_tempcool.DotSpan = 1;

            int counter = 0;
            // || File.Exists(logDir + counter.ToString("000") + "_usart_exception.log")
            while (File.Exists(logDir + counter.ToString("000") + "_usart_data.log"))
                counter++;
            if (counter > 0)
            {
                counter--;
                fileClient = new FileNetworkClient(logDir + counter.ToString("000") + "_usart_data.log") { ShortMode = true };
                DataWrapper[] all = fileClient.ReadDataWrappers(true);

                int lastSpeed = 0;
                int lastSoc = 0;
                int lastTempbat = 0;
                int lastTempcool = 0;

                foreach (DataWrapper dw in all)
                {
                    switch (dw.SensorID)
                    {
                        case 0:
                            lastSpeed = dw.Value;
                            break;
                        case 1:
                            lastSoc = dw.Value;
                            break;
                        case 5:
                            lastTempbat = dw.Value;
                            break;
                        case 6:
                            lastTempcool = dw.Value;
                            break;
                    }
                    graphDisplay_speed.Values.Add(lastSpeed);
                    graphDisplay_soc.Values.Add(lastSoc);
                    graphDisplay_tempbat.Values.Add(lastTempbat);
                    graphDisplay_tempcool.Values.Add(lastTempcool);
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            prev = cur;
            cur = Keyboard.GetState();

            if (prev.IsKeyUp(Keys.F5) && cur.IsKeyDown(Keys.F5))
            {
                scroller.Elements.Clear();
                InitializeComponents(mainRenderer);
            }

            scroller.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            mainRenderer.Clear();
            scroller.Draw(mainRenderer, gameTime);
            mainRenderer.Present();
        }
    }
}
